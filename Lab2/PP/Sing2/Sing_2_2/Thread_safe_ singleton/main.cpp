//
//  main.cpp
//  Thread_safe_ singleton
//
//  Created by Yura Slipenkyi on 5/13/20.
//  Copyright © 2020 Yura Slipenkyi. All rights reserved.
//

#include <iostream>
#include <pthread.h>

#define threads_number 10

using namespace std;

class Shop
{
    Shop()
    {
        cout << "Create\n";
    }
public:
    static Shop * ShopSingletonInstance;
    static pthread_mutex_t Mutex;
    static void * GetInstance(void *)
    {
        pthread_mutex_lock(&Mutex);
        if(ShopSingletonInstance == NULL)
        {
            ShopSingletonInstance = new Shop();
        }
        pthread_mutex_unlock(&Mutex);
        return ShopSingletonInstance;
    }
};
Shop * Shop::ShopSingletonInstance = NULL;
pthread_mutex_t Shop::Mutex = PTHREAD_MUTEX_INITIALIZER;



int main(void)
{
    pthread_t *Threads = new pthread_t[threads_number];
    pthread_attr_t *Threads_att = new pthread_attr_t[threads_number];
    for (int i = 0; i < threads_number; ++i)
    {
        pthread_attr_init(&Threads_att[i]);
        pthread_create(&Threads[i], &Threads_att[i], Shop::GetInstance, NULL);
    }
        
    for (int i = 0; i < threads_number; ++i)
    {
        pthread_join(Threads[i], 0);
    }
    
    delete [] Threads;
    delete [] Threads_att;

    return 0;
}
